# Unifi Video Motion MQTT Bridge

This software allows you to make the motion status of Unifi Video cameras available via MQTT.

## Installation

Use Docker. Either modify the included [docker-compose.dev.yml](docker-compose.dev.yml) file or run it directly:
```shell
docker run -v "/path/to/unifi/video/logs:/app/logs:ro" \
    -e "MQTT_URI=tcp://your.mqtt.broker.address:1883" \
    -e "MQTT_USERNAME=username" \ 
    -e "MQTT_PASSWORD=password" \
    registry.gitlab.com/derenderkeks/unifi-video-motion-mqtt-bridge
```

Other options: 
- `LOG_PATH`: Path to log files directory (set in the container by default)
- `USE_POLLING`: Poll log file instead of using events (default `false`)
- `MQTT_DISCOVERY_PREFIX`: Prefix for Homeassistant discovery, don't set it when you don't know what it does (default `homeassistant`)
- `MQTT_DISCOVERY_NODE_ID`: Unique ID in case you want to have multiple instances of this software (default none)

## License

[AGPL v3](LICENSE)
