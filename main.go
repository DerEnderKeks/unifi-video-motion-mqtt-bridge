package main

import (
	"bufio"
	"encoding/json"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/hpcloud/tail"
	"log"
	"os"
	"path"
	"regexp"
	"strings"
)

func getEnv(key, defaultValue string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	if len(defaultValue) == 0 {
		log.Fatal("Missing environment variable " + key)
	}
	return defaultValue
}

type discoveryConfig struct {
	Name        string `json:"name"`
	DeviceClass string `json:"device_class"`
	StateTopic  string `json:"state_topic"`
}

func main() {
	log.Println("Unifi Video Motion MQTT Bridge is starting...")
	MqttUri := getEnv("MQTT_URI", "")
	MqttUsername := getEnv("MQTT_USERNAME", "")
	MqttPassword := getEnv("MQTT_PASSWORD", "")
	LogPath := getEnv("LOG_PATH", "")
	MqttDiscoveryPrefix := getEnv("MQTT_DISCOVERY_PREFIX", "homeassistant")
	MqttDiscoveryNodeId := getEnv("MQTT_DISCOVERY_NODE_ID", "-")
	UsePolling := getEnv("USE_POLLING", "false")
	CameraLog := path.Join(LogPath, "camera.log")
	MotionLog := path.Join(LogPath, "motion.log")

	cameraFile, err := os.Open(CameraLog)
	if err != nil {
		log.Fatal(err)
	}
	defer cameraFile.Close()

	options := mqtt.NewClientOptions()
	options.AddBroker(MqttUri)
	options.SetUsername(MqttUsername)
	options.SetPassword(MqttPassword)
	client := mqtt.NewClient(options)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error())
	} else {
		log.Printf("Connected to MQTT broker at %s as %s\n", MqttUri, MqttUsername)
	}
	defer client.Disconnect(0)

	if MqttDiscoveryNodeId == "-" {
		MqttDiscoveryNodeId = ""
	} else {
		MqttDiscoveryNodeId = MqttDiscoveryNodeId + "/"
	}

	cameras := make(map[string]string)

	scanner := bufio.NewScanner(cameraFile)
	log.Println("Detecting cameras...")
	for scanner.Scan() {
		re := regexp.MustCompile(`\[uv\.camera\.task]\sCamera\[([A-Z0-9]+)\|(.*?)]\sInitTask\s-\sSTART`)
		match := re.FindStringSubmatch(scanner.Text())
		if match == nil {
			continue
		}
		if _, ok := cameras[match[1]]; !ok {
			cameras[match[1]] = match[2]
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	if token := client.Subscribe(
		MqttDiscoveryPrefix+"/binary_sensor/"+MqttDiscoveryNodeId+"+/config",
		0, cleanupHandler(cameras),
	); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error())
	}

	log.Println("Publishing configs...")
	for id, name := range cameras {
		publishConfig(client, MqttDiscoveryPrefix, MqttDiscoveryNodeId, id, name)
	}

	log.Println("Monitoring motion log...")
	t, err := tail.TailFile(MotionLog, tail.Config{
		Follow:   true,
		ReOpen:   true,
		Location: &tail.SeekInfo{Offset: 0, Whence: 2},
		Logger:   tail.DiscardingLogger,
		Poll:     UsePolling == "true",
	})
	if err != nil {
		log.Fatal(err)
	}
	for line := range t.Lines {
		re := regexp.MustCompile(`\[uv\.analytics\.motion]\s\[AnalyticsService]\s\[([A-Z0-9]+)\|(.*?)]\sMotionEvent\stype:(start|stop)`)
		match := re.FindStringSubmatch(line.Text)
		if match == nil {
			continue
		}
		if _, ok := cameras[match[1]]; !ok {
			cameras[match[1]] = match[2]
			publishConfig(client, MqttDiscoveryPrefix, MqttDiscoveryNodeId, match[1], match[2])
		}

		var status string
		if match[3] == "start" {
			status = "ON"
		} else {
			status = "OFF"
		}

		if token := client.Publish(
			MqttDiscoveryNodeId+"uvc-"+match[1]+"/state",
			0,
			true,
			status,
		); token.Wait() && token.Error() != nil {
			log.Printf("Failed to publish state for %s: %v\n", match[1], token.Error())
		}
	}
}

func publishConfig(client mqtt.Client, MqttDiscoveryPrefix string, MqttDiscoveryNodeId string, id string, name string) {
	config := discoveryConfig{
		Name:        name,
		DeviceClass: "motion",
		StateTopic:  MqttDiscoveryNodeId + "uvc-" + id + "/state",
	}
	configTopic := MqttDiscoveryPrefix + "/binary_sensor/" + MqttDiscoveryNodeId + "uvc-" + id + "/config"
	configText, err := json.Marshal(config)
	if err != nil {
		log.Fatal(err)
	}
	if token := client.Publish(configTopic, 0, true, configText); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error())
	}
	log.Printf("Published config for camera %s (%s)\n", id, name)
}

// deleting old configs for camera IDs that are no longer in the camera log file
func cleanupHandler(cameras map[string]string) func(client mqtt.Client, msg mqtt.Message) {
	return func(client mqtt.Client, msg mqtt.Message) {
		if msg.Retained() == false {
			return
		}
		id := msg.Topic()[:len(msg.Topic())-7]
		id = id[strings.LastIndex(id, "/")+5:]
		if _, ok := cameras[id]; !ok {
			client.Publish(msg.Topic(), 0, true, "").Wait()
			log.Printf("Deleted old config for camera %s\n", id)
		}
	}
}
