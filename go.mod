module gitlab.com/DerEnderKeks/UnifiVideoMotionMQTTBridge

go 1.14

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/hpcloud/tail v1.0.0
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
